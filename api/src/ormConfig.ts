import { MikroORM } from "@mikro-orm/core";
import path from "path";

const _prod = process.env.NODE_ENV === "production";

export default {
  entities: [path.join(__dirname, "./entities")],
  dbName: "dashboard_app",
  user: "tsiaizina",
  password: "dBaseAdmin!",
  type: "mysql",
  migrations: {
    path: path.join(__dirname, "./migrations"),
    tableName: "orm_migrations",
    pattern: /^[\w-]+\d+\.ts$/,
  },
  debug: !_prod,
} as Parameters<typeof MikroORM.init>[0];
