import { Entity, Property } from "@mikro-orm/core";
import { PrimaryKey } from "@mikro-orm/core";

@Entity()
export class Problem {
  @PrimaryKey()
  id!: number;

  @Property({ unique: true })
  description!: String;

  @Property()
  delay!: String;

  @Property()
  state!: "resolved" | "not resolved";
}
