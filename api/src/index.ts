// Not object oriented (useless of class)
// With usage of ORM (Mikro-orm)

import { Problem } from "./entities/Problem";
import { User } from "./entities/User";
import { MikroORM } from "@mikro-orm/core";
import express from "express";
import cors from "cors";
import ormConfig from "./ormConfig";
import bodyParser from "body-parser";
import argon2 from "argon2";
import mysql from "mysql";

const withORM = async () => {
  // Initialize ORM
  const orm = await MikroORM.init(ormConfig);
  await orm.getMigrator().up();

  // Usage of express
  const app = express();
  app.use(bodyParser.urlencoded({ extended: false }));

  // Cors (Cross-Origin Resource-Sharing)
  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true,
    })
  );

  // User register , hashed password
  app.post("/api/user/register", async function (req, res): Promise<Object> {
    const password = await argon2.hash(req.body.password);
    const create = orm.em.create(User, {
      username: req.body.username,
      password: password,
    });
    await orm.em.persistAndFlush(create);

    return res.send({ name: create.username, id: create.id });
  });

  // User login
  app.post("/api/user/login", async function (req, res): Promise<Object> {
    const find = await orm.em.findOne(User, {
      username: req.body.username,
    });

    if (!find) return res.status(404).send({ userError: true });
    else {
      const valid = await argon2.verify(
        find.password.toString(),
        req.body.password
      );
      return valid
        ? res.send({ valid: true, id: find.id, name: find.username })
        : res.status(404).send({ passError: true });
    }
  });

  // Create a problem
  app.post("/api/problem/create", async function (req, res): Promise<Object> {
    const create = orm.em.create(Problem, {
      description: req.body.description,
      delay: req.body.delay,
      state: req.body.state,
    });

    await orm.em.persistAndFlush(create);

    return res.send(create);
  });

  // Edit
  // Problem's state only could be edited
  app.put("/api/problem/edit/:id", async function (req, res): Promise<Object> {
    const choice = await orm.em.findOne(Problem, {
      id: parseInt(req.params.id),
    });

    await orm.em.nativeUpdate(
      Problem,
      { id: choice?.id },
      { state: req.body.state }
    );

    await orm.em.flush();

    return res.send({ id: choice?.id, state: choice?.state });
  });

  // Delete a problem
  app.delete(
    "/api/problem/delete/:id",
    async function (req, res): Promise<Boolean> {
      await orm.em.nativeDelete(Problem, { id: parseInt(req.params.id) });

      res.send({ deleted: true });

      return true;
    }
  );

  // Map all Problem
  app.get("/api/problem", async function (req, res): Promise<Problem[] | any> {
    const allProblem = await orm.em.find(Problem, {});

    return res.send(allProblem);
  });

  app.listen(3004, () => console.log("Server on 3004"));
};

// Launch withORM server
// withORM().catch((err) => console.log("Error", err));

// With usage of pure sql query
const withPureQuery = async () => {
  const db = mysql.createConnection({
    user: "tsiaizina",
    password: "dBase@Admin!",
    database: "dashboard_app",
  });

  const app = express();

  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true,
    })
  );

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  // All user
  app.get("/api/user", (req, res) => {
    const user = db.query("select * from user", (err, rows) => {
      return res.send(rows).end();
    });
    return user;
  });

  // User login
  app.post("/api/user/login", async (req, res) => {
    const login = db.query(
      "select * from user where username=? limit 1",
      [req.body.username],
      async (err, rows) => {
        if (err) throw err;
        if (!rows.length)
          return res
            .status(404)
            .send({ nameError: "Invalid username !" })
            .end();
        else {
          const valid = await argon2.verify(
            rows[0].password.toString(),
            req.body.password
          );
          if (!valid)
            return res
              .status(404)
              .send({ passError: "Invalid password !" })
              .end();
          else
            return res.send({ id: rows[0].id, name: rows[0].username }).end();
        }
      }
    );

    return login;
  });

  // User register
  app.post("/api/user/register", async (req, res) => {
    const register = db
      .query(
        "insert into user (username,password,created_at,updated_at) values (?,?,now(),now())",
        [req.body.username, await argon2.hash(req.body.password)]
      )
      .on("error", (err) => {
        throw err;
      })
      .on("result", () => {
        return res.send({ registered: true });
      });
    return register;
  });

  // Problem map
  app.get("/api/problem", (req, res) => {
    const problem = db.query("select * from problem", (err, rows) => {
      if (err) throw err;
      res.send(rows);
    });
    return problem;
  });

  // Problem add
  app.post("/api/problem/add", (req, res) => {
    const add = db.query(
      "insert into problem (description,delay,state) values (?,?,?)",
      [req.body.description, req.body.delay, req.body.state],
      (err) => {
        if (err) throw err;
        res.send({ added: true });
      }
    );
    return add;
  });

  // Problem edit
  app.put("/api/problem/edit/:id", (req, res) => {
    const edit = db.query(
      "update problem set state=? where id=?",
      [req.body.state, req.params.id],
      (err) => {
        if (err) throw err;
        res.send({ edited: true });
      }
    );
    return edit;
  });

  app.listen(3004, () => console.log("Server on 3004"));
};

withPureQuery().catch((err) => console.log("Error", err));
