import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { DashboardComponent } from "./other/dashboard";
import { LoginPage } from "./other/login";
import { Login } from "./pages/login";
import { Home } from "./pages/home";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact children={<Login />} />
        <Route path="/home" children={<Home />} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
