import { Button, Grid, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import './login.css'

export const LoginPage = () => {

    // const users = [
    //     {
    //         id: '1',
    //         name: 'Ravel',
    //         firstName: "Pey'pé",
    //         address: 'Ampasapito',
    //         phone: '0340123456'
    //     },
    //     {
    //         id: '2',
    //         name: 'Ravel',
    //         firstName: "Pey'pé",
    //         address: 'Ampasapito',
    //         phone: '0340123456'
    //     },
    //     {
    //         id: '3',
    //         name: 'Ravel',
    //         firstName: "Pey'pé",
    //         address: 'Ampasapito',
    //         phone: '0340123456'
    //     },
    //     {
    //         id: '4',
    //         name: 'Ravel',
    //         firstName: "Pey'pé",
    //         address: 'Ampasapito',
    //         phone: '0340123456'
    //     }
    // ]

     const [login, setLogin] = useState('null')

    const handleChange = (e) => {
        const { name, value } = e.target
        setLogin({ ...login, [name]: value })
        console.log(login)
    }

    const handleSubmit = () => {

    }

    const onConnexion = () => {
        window.location.assign('/dashboard/participants')
    }

    return (
        <div>
            <Grid container direction='row' justify='space-between'>
                <Grid item md={5}>
                    <img className='loginImg' src='/images/105-freelancer.png' alt='loginImg' />

                </Grid>
                <Grid className='loginSection' item md={6}>
                    <Grid container direction='column' spacing={8}>
                        <Grid item>
                            <h1>Ataomad React Course</h1>
                            <p>Connectez-vous</p>
                        </Grid>
                        <Grid item>
                            <form>
                                <Grid container direction='column' spacing={3} alignItems='flex-start'>
                                    <Grid item>
                                        <TextField className='userTextInput' type='text' variant='outlined' name='user' label="Nom d'utilisateur" value={login.user} onChange={handleChange} />
                                    </Grid>
                                    <Grid item>
                                        <TextField className='userTextInput' type='password' variant='outlined' name='password' label="Mot de passe" value={login.password} onChange={handleChange} />
                                    </Grid>

                                    <Grid item>
                                        <Button className='btnLogin' variant='contained' color='primary' onClick={onConnexion} >CONNEXION</Button>
                                    </Grid>
                                </Grid>

                            </form>
                        </Grid>
                    </Grid>


                </Grid>
            </Grid>
        </div>
    )
}