import React from 'react'
import { Avatar, Grid, List, ListItem } from '@material-ui/core'
import { BrowserRouter, NavLink, Route, Switch, useRouteMatch } from 'react-router-dom'
import { ParticipantComponent } from './participant'
import { ProblemComponent } from './problem'

import './dashboard.css'
export const DashboardComponent = () => {

    const deconnection = () => {
        window.location.href = '/'
    }

    return (
        <div>
            <BrowserRouter>
                <Grid container direction='row'>

                    <Grid className='sideMenu' item md={2}>
                        <h1>CRA</h1>

                        <List>
                            <ListItem>
                                <NavLink className='dashNavLink' to='/dashboard/participants' exact>
                                    Participants
                            </NavLink>

                            </ListItem>

                            <ListItem>
                                <NavLink className='dashNavLink' to='/dashboard/problems' exact>
                                    Problem
                            </NavLink>

                            </ListItem>
                        </List>

                    </Grid>
                    <Grid item md={9}>
                        <Grid container direction='column'>
                            <Grid item>
                                <Grid container direction='row' justify='flex-end' spacing={2} alignItems='center'>
                                    <Grid item >
                                        <Avatar src='/images/avatar.png' onClick={deconnection} />
                                    </Grid>
                                    <Grid item>
                                        <p>Pey'pé Ravel</p>
                                    </Grid>

                                </Grid>

                            </Grid>
                            <Grid item>
                                <Route path='/dashboard/'>
                                    <Switch>
                                        <Route exact path='/dashboard/' component={ParticipantComponent} />
                                        <Route path='/dashboard/participants' component={ParticipantComponent} />
                                        <Route path='/dashboard/problems' component={ProblemComponent} />
                                    </Switch>
                                </Route>

                            </Grid>
                        </Grid>







                    </Grid>

                </Grid>

            </BrowserRouter>
        </div>
    )
}