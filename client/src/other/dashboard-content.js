import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import { ParticipantComponent } from './participant'
import { ProblemComponent } from './problem'

export const DashboardContent = () => {
    return (
        <div>

            <Switch>
                <Route exact path='/dashboard/participants' component={ParticipantComponent} />
                <Route path='/dashboard/problems' component={ProblemComponent} />
            </Switch>
        </div>
    )
}