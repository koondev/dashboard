import React, { useState } from 'react'
import { Button, Grid, InputLabel, MenuItem, Select, Table, TableBody, TableCell, TableHead, TableRow, TextField } from '@material-ui/core'
import './problem.css'

export const ProblemComponent = () => {
    const problemes = [
        {
            id: '1',
            description: "problème de création de projet react",
            duree: '20 min',
            statut: 'Résolu'
        },
        {
            id: '2',
            description: "problème d'installation du module material-ui",
            duree: '60 min',
            statut: 'Résolu'
        },
        {
            id: '3',
            description: "Mis en place de route",
            duree: '60 min',
            statut: 'Non résolu'
        },
    ]

    const [problem, setProblem] = useState(problemes)
    const [nwProblem, setNwProblem] = useState()

    const handleChange = (e) => {
        const { name, value } = e.target

        setNwProblem({ ...nwProblem, [name]: value })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const pblm = problem.slice()
        console.log(nwProblem)
        pblm.push({ id: pblm.length + 1, description: nwProblem.description, duree: nwProblem.duree + ' min', statut: nwProblem.statut })
        setProblem(pblm)

    }
    return (
        <div>
            <Grid container direction='column' spacing={4} style={{ paddingLeft: '32px' }}>
                <Grid item>
                    <h2 style={{ textAlign: 'start' }}>Liste des problèmes pendant la mise en pratique</h2>
                </Grid>

                <Grid item>
                    <Grid container direction='row' justify='space-between'>
                        <Grid item md='8'>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>#</TableCell>
                                        <TableCell>Description</TableCell>
                                        <TableCell>Durée du plantage</TableCell>
                                        <TableCell>Statut</TableCell>

                                    </TableRow>
                                </TableHead>

                                <TableBody>
                                    {problem.map(item => {
                                        return (
                                            <TableRow>
                                                <TableCell>{item.id}</TableCell>
                                                <TableCell>{item.description}</TableCell>
                                                <TableCell>{item.duree}</TableCell>
                                                <TableCell>{item.statut}</TableCell>

                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>
                        </Grid>

                        <Grid item md={3}>
                            <Grid container direction='column' alignItems='flex-start' spacing={2}>
                                <Grid item>
                                    <h4>Nouveau problème rencontré</h4>
                                </Grid>
                                <Grid item>
                                    <TextField className='problemInput' type='text' label='Description' name="description" variant='outlined' value={nwProblem?.description} onChange={handleChange} />
                                </Grid>
                                <Grid item>
                                    <TextField className='problemInput' type='number' label='Durée de plantage' name="duree" value={nwProblem?.duree} onChange={handleChange} variant='outlined' />
                                </Grid>
                                <Grid item>

                                    <Select className='problemInput' variant='outlined' label='Statut' name="statut" value={nwProblem?.statut} onChange={(e) => handleChange(e)} >
                                        <MenuItem value='Résolu'>Résolu</MenuItem>
                                        <MenuItem value='Non résolu'>Non résolu</MenuItem>
                                    </Select>
                                </Grid>
                                <Grid item>
                                    <Button variant='contained' style={{ background: '#5F235A', color: '#fff' }} onClick={handleSubmit}>Valider</Button>
                                </Grid>

                            </Grid>
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>


        </div>
    )
}