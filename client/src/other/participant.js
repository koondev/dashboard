import React from 'react'
import { Grid, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
export const ParticipantComponent = () => {
    const learners = [
        {
            id: '1',
            name: 'Ravel',
            firstName: "Pey'pé",
            address: 'Ampasapito',
            phone: '0340123456'
        },
        {
            id: '2',
            name: 'Ravel',
            firstName: "Pey'pé",
            address: 'Ampasapito',
            phone: '0340123456'
        },
        {
            id: '3',
            name: 'Ravel',
            firstName: "Pey'pé",
            address: 'Ampasapito',
            phone: '0340123456'
        },
        {
            id: '4',
            name: 'Ravel',
            firstName: "Pey'pé",
            address: 'Ampasapito',
            phone: '0340123456'
        }
    ]
    return (
        <div>
            <Grid container direction='column' spacing={4} style={{ paddingLeft: '32px' }}>
                <Grid item>
                    <h2 style={{ textAlign: 'start' }}>Liste des participants au cours</h2>
                </Grid>

                <Grid item>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>#</TableCell>
                                <TableCell>Nom</TableCell>
                                <TableCell>Prénom</TableCell>
                                <TableCell>Adresse</TableCell>
                                <TableCell>Contact</TableCell>
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {learners.map(item => {
                                return (
                                    <TableRow>
                                        <TableCell>{item.id}</TableCell>
                                        <TableCell>{item.name}</TableCell>
                                        <TableCell>{item.firstName}</TableCell>
                                        <TableCell>{item.address}</TableCell>
                                        <TableCell>{item.phone}</TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>

        </div>
    )
}