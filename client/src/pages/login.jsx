import React, { useState } from "react";
import { Field, Form, Formik } from "formik";
import { useHistory } from "react-router-dom";
import { Alert } from "@material-ui/lab";
import {
  Button,
  Grid,
  makeStyles,
  TextField,
  Container,
  CircularProgress,
} from "@material-ui/core";
import axios from "axios";
import { _url } from "../url";

const styles = makeStyles((theme) => ({
  text: {
    fontSize: 25,
    lineHeight: 3,
    fontWeight: 1000,
  },

  text_i: {
    textAlign: "end",
    color: "grey",
  },
}));

export const Login = () => {
  const history = useHistory();

  sessionStorage.length && history.push("/home");
  const classes = styles();
  const [error, setError] = useState({ state: false, msg: "" });

  return (
    <>
      <Grid container direction="row" justify="space-between">
        <Grid style={{ height: 630 }} item xs={12} md={6}>
          <img
            style={{ width: "100%", height: "100%" }}
            src="/images/105-freelancer.png"
            alt=""
          />
        </Grid>
        <Grid item md={6} xs={12} style={{ paddingBottom: 30 }}>
          <Grid style={{ width: "100%" }} container direction="column">
            <Grid item style={{ paddingBottom: 20, alignSelf: "center" }}>
              <p className={classes.text}>
                Simple React App <br />
              </p>
            </Grid>

            <Grid item>
              <Container
                style={{
                  width: 370,
                  backgroundColor: "grey",
                  padding: 30,
                  borderRadius: 10,
                }}
              >
                <p>Connectez-vous !</p>
                <Formik
                  initialValues={{ username: "", password: "" }}
                  onSubmit={async (values, actions) => {
                    if (!values.username || !values.password) {
                      actions.setSubmitting(false);
                      setError({ state: true, msg: "All input required !" });
                    } else {
                      try {
                        const login = await axios.post(`${_url}/user/login`, {
                          username: values.username,
                          password: values.password,
                        });
                        if (login.data) {
                          sessionStorage.setItem(
                            "id",
                            login.data.id.toString()
                          );
                          sessionStorage.setItem("name", login.data.name);
                          history.push("/home");
                        }
                      } catch (err) {
                        if (err.response) {
                          setError({
                            state: true,
                            msg:
                              err.response.data.nameError ||
                              err.response.data.passError,
                          });
                        }
                      }
                    }
                  }}
                >
                  {({ handleSubmit, isSubmitting }) => (
                    <Form onSubmit={handleSubmit}>
                      <Grid direction="column" spacing={2} container>
                        <Grid item>
                          <Field name="username">
                            {({ field }) => (
                              <TextField
                                onChangeCapture={() => setError(false)}
                                variant="outlined"
                                label="Username"
                                fullWidth
                                {...field}
                              />
                            )}
                          </Field>
                        </Grid>
                        <Grid item>
                          <Field name="password">
                            {({ field }) => (
                              <TextField
                                onChangeCapture={() => setError(false)}
                                type="password"
                                variant="outlined"
                                label="Password"
                                fullWidth
                                {...field}
                              />
                            )}
                          </Field>
                        </Grid>
                        {error.state && (
                          <Grid item>
                            <Alert severity="error">{error.msg}</Alert>
                          </Grid>
                        )}
                        <Grid item style={{ alignSelf: "center" }}>
                          <Button
                            disabled={isSubmitting}
                            variant="contained"
                            type="submit"
                            color="primary"
                          >
                            {isSubmitting ? (
                              <CircularProgress style={{ color: "white" }} />
                            ) : (
                              "Login"
                            )}
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  )}
                </Formik>
              </Container>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
