import {
  Box,
  Container,
  Paper,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  Table,
  Button,
  OutlinedInput,
  Grid,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import "animate.css";
import React, { useEffect, useState } from "react";
import { Edit } from "@material-ui/icons";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import { _url } from "../../url";

const styles = makeStyles(() => ({
  entrance: {
    animation: "fadeIn 1s",
  },
}));

export const Problemes = () => {
  const [rows, setRows] = useState();
  const [added, setAdded] = useState(false);
  const classes = styles();
  const [newP, setNewP] = useState(false);
  const [error, setError] = useState(false);
  const [load, setLoad] = useState(false);
  const [probVal, setProblem] = useState({ desc: "", delay: "", state: "" });

  const data = async () => {
    try {
      const res = await axios.get(`${_url}/problem`);

      setRows(res.data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    data();
  }, [added]);

  const handleChange = (e) => {
    const name = e.target.name;
    const input = e.target.value;
    setError(false);
    setProblem({ ...probVal, [name]: input });
  };

  const validation = async () => {
    if (!probVal.desc || !probVal.delay || !probVal.state) {
      setError(true);
    } else {
      setLoad(true);
      try {
        const res = await axios.post(`${_url}/problem/add`, {
          description: probVal.desc,
          delay: probVal.delay,
          state: probVal.state,
        });

        (res.data?.added && setAdded(true)) ||
          setLoad(false) ||
          setNewP(false) ||
          setProblem({});
      } catch (err) {
        console.log(err);
      }
    }
  };

  setTimeout(() => {
    added && setAdded(false);
  }, 4000);

  return (
    <Box>
      <Grid container direction="row" alignItems="center">
        <>
          <Grid item xs={12}>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell align="center">ID</TableCell>
                    <TableCell align="center">Description</TableCell>
                    <TableCell align="center">Delay</TableCell>
                    <TableCell align="center">State</TableCell>
                    <TableCell width={50}></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows?.map((item) => {
                    return (
                      <TableRow key={item.id}>
                        <TableCell align="center">{item.id}</TableCell>
                        <TableCell align="center">{item.description}</TableCell>
                        <TableCell align="center">{item.delay}</TableCell>
                        <TableCell align="center">{item.state}</TableCell>
                        <TableCell align="center">
                          <Edit />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </Grid>
        </>
      </Grid>
      <Box textAlign="center" paddingBottom={4}>
        {added && (
          <>
            <br />
            <p style={{ color: "green" }}>Added !</p>
          </>
        )}
        <Button
          variant="contained"
          color="primary"
          style={{ marginTop: 10 }}
          onClick={() => setNewP(!newP)}
        >
          {newP ? (
            "Cancel"
          ) : (
            <Box display="flex" alignItems="center">
              <p>New problem</p>
            </Box>
          )}
        </Button>
      </Box>
      {newP && (
        <Box
          className={classes.entrance}
          display="flex"
          flexDirection="column"
          paddingBottom={4}
        >
          <Container maxWidth="xs">
            <p style={{ paddingBottom: 15 }}>New Problem</p>
            <OutlinedInput
              value={probVal.desc}
              name="desc"
              onChange={handleChange}
              placeholder="Description"
              fullWidth
            />
            <OutlinedInput
              style={{ marginBottom: 10, marginTop: 10 }}
              placeholder="Delay"
              fullWidth
              name="delay"
              value={probVal.delay}
              onChange={handleChange}
            />
            <OutlinedInput
              onChange={handleChange}
              value={probVal.state}
              name="state"
              placeholder="State"
              fullWidth
            />
            {error && (
              <Alert style={{ marginTop: 10 }} severity="error">
                All input are required !
              </Alert>
            )}
            <Button
              style={{ marginTop: 10 }}
              variant="contained"
              color="primary"
              onClick={validation}
            >
              {load ? (
                <CircularProgress style={{ color: "white" }} />
              ) : (
                "Validate"
              )}
            </Button>
          </Container>
        </Box>
      )}
    </Box>
  );
};
