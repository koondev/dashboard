import React, { useState } from "react";
import { Box, Container, Grid, makeStyles, Avatar } from "@material-ui/core";
import { Menu, Search } from "@material-ui/icons";
import { BrowserRouter, Switch, Route, useHistory } from "react-router-dom";

import { Problemes } from "./component/probleme";

const styles = makeStyles((theme) => ({
  brand: {
    color: "white",
    fontSize: 30,
    paddingLeft: 20,
  },
  menu: {
    fontSize: 45,
    color: "white",
    cursor: "pointer",
  },
}));

export const Home = () => {
  const history = useHistory();
  let name;
  const classes = styles();

  sessionStorage.length
    ? (name = sessionStorage.getItem("name"))
    : history.push("/");

  setTimeout(() => {
    sessionStorage.removeItem("name");
  }, 500);
  return (
    <BrowserRouter basename="/home">
      <Container style={{ backgroundColor: "black", height: 50 }} maxWidth="xl">
        <Box flexDirection="row" display="flex" justifyContent="space-between">
          <Box alignItems="center" display="flex">
            <Menu className={classes.menu} />
            <Box className={classes.brand}>Arc</Box>
          </Box>
          <Box alignSelf="center" color="white">
            <Grid item xs={12}>
              <Box alignItems="center" display="flex">
                <Search
                  style={{
                    color: "white",
                    fontSize: 35,
                    backgroundColor: "grey",
                    paddingLeft: 5,
                  }}
                />

                <input
                  type="text"
                  style={{
                    backgroundColor: "grey",
                    height: 33,
                    paddingLeft: 5,
                    fontSize: 20,
                    border: "none",
                  }}
                />
              </Box>
            </Grid>
          </Box>
          <Box alignItems="center" display="flex">
            <Avatar style={{ fontSize: 50 }} alt="" src="/images/avatar.png" />
            <Box style={{ color: "white", paddingLeft: 10 }}>{name}</Box>
          </Box>
        </Box>
      </Container>

      <Switch>
        <Route path="/" exact children={<Problemes />} />
        {/* <Route path="/historiques"  children={""} />
        <Route path="/notions" children={""} /> */}
      </Switch>
    </BrowserRouter>
  );
};
